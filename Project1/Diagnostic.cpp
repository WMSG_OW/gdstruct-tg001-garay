#include <iostream>
#include <time.h>
#include <string>

using namespace std;

void randNumAssign(int numbers[], int size)
{
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() %  69;
	}
}

void ascendingSort(int numbers[], int size)
{
	int i,j;
	int temp;
	for (i = 1; i <= size - 1; i++)
	{
		for (j = 0 ; j <= size - 2; j++)
		{
			if (numbers[j] > numbers[i])
			{
				temp = numbers[j];
				numbers[j] = numbers[i];
				numbers[i] = temp;
			}
		}
	}
}

void descendingSort(int numbers[], int size)
{
	int i, j;
	int temp;
	for (i = 1; i <= size - 1; i++)
	{
		for (j = 0; j <= size - 2; j++)
		{
			if (numbers[j] < numbers[i])
			{
				temp = numbers[j];
				numbers[j] = numbers[i];
				numbers[i] = temp;
			}
		}
	}
}

void search(int numbers[], int size, int numSearch)
{
	int counter = 1;
	for (int i = 0; i < size; i++)
	{
		if (numSearch != numbers[i])
		{
			counter++;
		}
		else if (numSearch == numbers[i])
		{
			cout << "Your number is " << numSearch;
			cout << " and it is found after " << counter << " counts ";
		}
	}
}

int main()
{
	int numArray[10];
	srand(time(NULL));

	// Array Creation and number assignment from 1 - 69
	randNumAssign(numArray, 10);

	for (int i = 0; i < 10; i++)
	{
		cout << numArray[i] << endl; 
	}
	system("pause");
	system("cls");
	// Sort according to user (Maybe Ascending or Descending

	char choice;

	cout << "Do you want to arrange it Ascending or Descending?? (a for ascending d for descending)" << endl; 
	cin >> choice;

	if (choice == 'a')
	{
		ascendingSort(numArray, 10);
		cout << "Your Ascending sorted array is " << endl;
			for (int i = 0; i < 10; i++)
			{
				cout << numArray[i] << " ";
			} 
	}
	else if (choice == 'd')
	{
		descendingSort(numArray, 10);
		cout << "Your Descending sorted array is " << endl;
		for (int i = 0; i < 10; i++)
		{
			cout << numArray[i] << " ";
		}
	}

	system("pause");
	system("cls");

    //Linear search if the value exists within the array  

	int searching;

	cout << "Input the value you want to search for...." << endl;
	cin >> searching;

	search(numArray, 10, searching);

	system("pause");
	return 0;
}